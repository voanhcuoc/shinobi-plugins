#!/bin/bash
DIR=$(dirname $0)
echo "Removing existing Node.js modules..."
rm -rf $DIR/node_modules

nonInteractiveFlag=false

if [ ! -e "$DIR/conf.json" ]; then
	dontCreateKeyFlag=false
    echo "Creating conf.json"
    cp $DIR/conf.sample.json $DIR/conf.json
else
    echo "conf.json already exists..."
fi

if [ "$dontCreateKeyFlag" = false ]; then
	echo "Adding Random Plugin Key to Main Configuration"
	node $DIR/../../tools/modifyConfigurationForPlugin.js platerecognizer key=$(head -c 64 < /dev/urandom | sha256sum | awk '{print substr($1,1,60)}')
fi

if [ -d "$DIR/open-lpr" ]; then
  echo "The 'open-lpr' folder already exists. Do you want to continue with 'docker-compose up -d'?"
  echo "(y)es or (N)o"
  read answer
  if [[ "$answer" =~ ^[Yy]$ ]]; then
    cd $DIR/open-lpr
    docker-compose up -d
  else
    echo "Aborted"
  fi
else
  git clone https://github.com/faisalthaheem/open-lpr
  cd $DIR/open-lpr
  docker-compose up -d
fi
