# OpenLPR

This utilizes OpenLPR made by Faisal Thaheem. https://github.com/faisalthaheem/open-lpr.

Document needs to be updated.

## Downloading and Installing the Plugin

1. Login to your Shinobi's Superuser panel.
    - https://YOUR_SHINOBI/super
2. Open the **"Plugin Manager"** tab.
3. In the listing select **"License Plate Recognition (open-lpr 2023-03-28)"** to Download the plugin.
4. Hit `Run Installer` for the newly downloaded plugin.
5. Once Installed click "Enable" and restart Shinobi.
6. Reopen the Plugin Manager and run the `Test License Plate Detector`.
7. Now go ahead and login to the main Dashboard to setup a Monitor! You can follow these guides for object detection.
    - https://YOUR_SHINOBI/
    - [Watch this video for a visual guide with some extra tips](https://youtu.be/Vk_5hlSQeV0?t=433)


[Learn more about Downloading and Installing Plugins here](https://docs.shinobi.video/plugin/install)
