#!/bin/bash

# Check for the existence of ./build/Release/addon.node
if [ ! -f ./build/Release/addon.node ]; then
    echo "addon.node not found. Building..."
    node-gyp configure build --verbose
else
    echo "addon.node exists."
fi

# Create config_files directory if it doesn't exist
if [ ! -d config_files ]; then
    mkdir config_files
fi

cd config_files

# Download yolov5s.onnx if it doesn't exist
if [ ! -f yolov5s.onnx ]; then
    curl -O https://cdn.shinobi.video/weights/yolov5-objectdetection/config_files/yolov5s.onnx
fi

# Download classes.txt if it doesn't exist
if [ ! -f classes.txt ]; then
    curl -O https://cdn.shinobi.video/weights/yolov5-objectdetection/config_files/classes.txt
fi

echo "Done! Run the Test!"
