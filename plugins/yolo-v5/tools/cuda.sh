#!/bin/sh
echo "------------------------------------------"
echo "-- Installing CUDA Toolkit and CUDA DNN --"
echo "------------------------------------------"
# Install CUDA Drivers and Toolkit
if [ -x "$(command -v apt)" ]; then
    # CUDA Toolkit
    wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin --no-verbose
    sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
    echo "Downloading CUDA Toolkit..."
    wget https://developer.download.nvidia.com/compute/cuda/11.2.0/local_installers/cuda-repo-ubuntu2004-11-2-local_11.2.0-460.27.04-1_amd64.deb -O cuda.deb --no-verbose
    sudo dpkg -i cuda.deb
    sudo apt-key add /var/cuda-repo-ubuntu2004-11-2-local/7fa2af80.pub
    sudo apt-get update
    sudo apt-get -y install cuda-toolkit-11-2

    # Driver
    echo "Installing nvidia-driver-515-server"
    sudo apt install nvidia-driver-515-server -y

    # Install CUDA DNN
    wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/libcudnn8_8.1.1.33-1+cuda11.2_amd64.deb -O cuda-dnn.deb --no-verbose
    sudo dpkg -i cuda-dnn.deb
    wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/libcudnn8-dev_8.1.1.33-1+cuda11.2_amd64.deb -O cuda-dnn-dev.deb --no-verbose
    sudo dpkg -i cuda-dnn-dev.deb
    echo "-- Cleaning Up --"
    # Cleanup
    sudo rm cuda.deb
    sudo rm cuda-dnn.deb
    sudo rm cuda-dnn-dev.deb
fi
if [ -x "$(command -v yum)" ]; then
    sudo yum-config-manager --add-repo http://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-rhel7.repo
    sudo yum clean all
    sudo yum -y install nvidia-driver-latest-dkms cuda-toolkit*
    sudo yum -y install cuda-drivers
    curl https://cdn.shinobi.video/installers/libcudnn7-7.6.5.33-1.cuda10.2.x86_64.rpm -o cuda-dnn.rpm
    sudo yum -y localinstall cuda-dnn.rpm
    curl https://cdn.shinobi.video/installers/libcudnn7-devel-7.6.5.33-1.cuda10.2.x86_64.rpm -o cuda-dnn-dev.rpm
    sudo yum -y localinstall cuda-dnn-dev.rpm
    echo "-- Cleaning Up --"
    sudo rm cuda-dnn.rpm
    sudo rm cuda-dnn-dev.rpm
fi

echo "------------------------------"
echo "Reboot is required. Do it now?"
echo "------------------------------"
echo "(y)es or (N)o. Default is No."
read rebootTheMachineHomie
if [ "$rebootTheMachineHomie" = "y" ] || [ "$rebootTheMachineHomie" = "Y" ]; then
    sudo reboot
fi
