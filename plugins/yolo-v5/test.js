process.on('uncaughtException', function (err) {
    console.error(`Uncaught Exception occured! ${new Date()}`);
    console.error(err.stack);
});
const fs = require('fs').promises;
const { spawn } = require('child_process');
const async = require('async');
const {
    drawDetections,
    loadClassNames,
    releaseNet,
    loadNet,
    detect
} = require('./libs/addon.js');
let classNames;
const VIDEO_URL = 'https://cdn.shinobi.video/videos/people.mp4';
const worker = async (task, callback) => {
    const currentTime = new Date().getTime()
    const detectResponse = await detect(task.frameBuffer, task.detectOptions);
    detectResponse.forEach((matrix) => {
        matrix.tag = classNames[matrix.class_id];
    });
    console.log(`Frame ${task.i} detected in ${(new Date().getTime() - currentTime) / 1000}s`);
    console.log(detectResponse);
    callback();
};
const queue = async.queue(worker, 1);
const processFramesFromVideo = async () => {
    return new Promise(async (resolve, reject) => {
        let frameData = [];
        let i = 0;
        const ffmpeg = spawn('ffmpeg', [
            '-re',
            '-i', VIDEO_URL,
            '-f', 'image2pipe',
            '-vf', 'fps=10',  // Extract 1 frame per second (you can adjust this)
            '-c:v', 'mjpeg',
            '-'
        ]);

        ffmpeg.stdout.on('data', async (chunk) => {
            frameData.push(chunk);

            if((chunk[chunk.length-2] === 0xFF && chunk[chunk.length-1] === 0xD9)){
                const frameBuffer = Buffer.concat(frameData);
                ++i;
                const task = {
                    frameBuffer,
                    i,
                    detectOptions: {
                        inputWidth: 640.0,
                        inputHeight: 640.0,
                        scoreThreshold: 0.2,
                        nmsThreshold: 0.4,
                        confidenceThreshold: 0.4,
                    }
                };
                queue.push(task);
                frameData = [];
            }
        });

        ffmpeg.on('close', (code) => {
            if (code !== 0) {
                reject(new Error(`ffmpeg exited with code ${code}`));
            } else {
                resolve();
            }
        });
    });
};

const main = async () => {
    const startTime = new Date().getTime();
    const pathToOnnxModel = `${__dirname}/config_files/yolov5s.onnx`
    const pathToClassesFile = `${__dirname}/config_files/classes.txt`
    classNames = await loadClassNames(pathToClassesFile)
    await loadNet(false,pathToOnnxModel)
    await processFramesFromVideo();
    const endTime = new Date().getTime();
    const inferenceTime = (endTime - startTime) / 1000;
    console.log(`Test done in ${inferenceTime}s!`);
    await releaseNet()
};

main().then(() => {
    process.exit();
}).catch(console.error);
