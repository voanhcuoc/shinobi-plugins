//
// Shinobi - Chooch Image Chat Plugin
// Copyright (C) 2023 Moe Alam, moeiscool
//
// # Donate
//
// If you like what I am doing here and want me to continue please consider donating :)
// PayPal : paypal@m03.ca
//
// Base Init >>
var fs = require('fs').promises;
var config = require('./conf.json')
var s
const {
  workerData
} = require('worker_threads');
if(workerData && workerData.ok === true){
    try{
        s = require('../pluginWorkerBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginWorkerBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'WORKER : Plugin start has failed. pluginBase.js was not found.')
        }
    }
}else{
    try{
        s = require('../pluginBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'Plugin start has failed. pluginBase.js was not found.')
        }
    }
}
// Base Init />>
const sharp = require('sharp');
const detectObjects = require('./ObjectDetectors.js')(config);
const ImageChatPredictor = require('chooch');
const hostName = config.choochEndpoint || 'https://chat-api.chooch.ai';
const apiKey = config.choochApiKey || 'xxxxxxxxx-xxxxx-xxxxxxx-xxxxxxx';
const modelId = "chooch-image-chat-3";
const predictor = new ImageChatPredictor(apiKey, modelId, hostName);
const parameters = {
    prompt: config.choochFramePrompt || "Assume the person is in a general store. Is someone concealing an item? Your response can be : Yes, Very Yes, No, or Mega No."
};

s.detectObject = async function(buffer,d,tx,frameLocation,callback){
    const resp = await detectObjects(buffer);
    var results = resp.data
    if(results[0]){
        var mats = []
        console.log(`new frame`,'-------')
        var n = 0;
        for (const matrix of results.filter(matrix => matrix.class === 'person')) {
            ++n;
            if(matrix.bbox[2] > 20 && matrix.bbox[3] > 20){
                const padding = 20;
                const bbox = matrix.bbox.map(x => parseInt(x))
                const croppedBuffer = await sharp(buffer)
                    .extract({
                        left: bbox[0],
                        top: bbox[1],
                        width: bbox[2],
                        height: bbox[3]
                    })
                    .extend({
                        top: padding,
                        bottom: padding,
                        left: padding,
                        right: padding,
                    })
                    .toBuffer();
                    console.log(`Person Found ${n}, Attempting Prediction...`)
                const prediction = await predictor.predict(parameters, croppedBuffer);
                console.log(`prediction`,prediction)
                if (prediction === 'Yes') {
                    console.log(`matrix PASSED`,matrix)
                    mats.push({
                        x: matrix.bbox[0],
                        y: matrix.bbox[1],
                        width: matrix.bbox[2],
                        height: matrix.bbox[3],
                        tag: matrix.class,
                        confidence: matrix.score,
                    });
                }
            }
        }
        var isObjectDetectionSeparate = d.mon.detector_use_detect_object === '1'
        var width = parseFloat(isObjectDetectionSeparate  && d.mon.detector_scale_y_object ? d.mon.detector_scale_y_object : d.mon.detector_scale_y)
        var height = parseFloat(isObjectDetectionSeparate  && d.mon.detector_scale_x_object ? d.mon.detector_scale_x_object : d.mon.detector_scale_x)
        tx({
            f:'trigger',
            id:d.id,
            ke:d.ke,
            details:{
                plug:config.plug,
                name:'ChoochImageChat',
                reason:'object',
                matrices:mats,
                imgHeight:width,
                imgWidth:height,
                time: resp.time
            },
            frame: buffer
        })
    }
    callback()
}
