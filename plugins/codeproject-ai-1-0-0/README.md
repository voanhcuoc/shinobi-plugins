# Code Project AI

Proxy layer between Shinobi Video and Code Projec tAI Server

## Features

One stop shop plugin for all CodeProject AI models:

- Object detection
- Face detection
- Scene classifier
- Custom Model
- Face recognition

Automatically send frames to the relevant model based on the configuration on the CodeProject AI server.

Interact with Face Manager to register and unregister faces.

Event message special properties:

- person - When face is being identify, will indicate the face name from face manager (on top of the tag that represents the image)
- duration - time took the sever to process the request (including upload file)

## Configuration

```bash
nano conf.json
```

Here is a sample of a Host configuration for the plugin,
Since the Pplugin is a proxy to the CodeProject AI server, just update the relevant details within `api`:

 - `host` - Hostname or IP to access Code Project AI Server
 - `port` - Port to access Code Project AI Server
 - `isSSL` - Choose if you want to use SSL or not
 - `apiKey` - Set the API Key in case defined in CodeProject AI Server
 - `minConfidence` - Set the minimum confidence level for the CodeProject AI for face recognition

```jsonc
{
  "plug": "CodeProject-AI",        
  "host": "localhost",             
  "port": 8080,                    
  "key": "CodeProject-AI",         
  "mode": "client",                
  "type": "detector",              
  "enabled": false,                
  "api": {
     "host": "HOSTNAME OR IP",  
     "port": 5000,                
     "isSSL": false,              
     "apiKey": "SECUREDKEY",
     "minConfidence": 0.7  
  }
}
```
