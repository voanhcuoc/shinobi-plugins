var fs = require('fs').promises
const {
    createReadStream
} = require('fs')
var fileUpload = require('express-fileupload')
module.exports = async function(s,config,lang,app,io){
    if(!config.facesFolder || !config.unknownFacesFolder){
        return console.error('Must define `facesFolder` and `unknownFacesFolder` in conf.json!')
    }
    const facesFolder = s.checkCorrectPathEnding(config.facesFolder)
    const unknownFacesFolder = s.checkCorrectPathEnding(config.unknownFacesFolder)
    try{
        await fs.mkdir(facesFolder)
    }catch(err){}
    try{
        await fs.mkdir(unknownFacesFolder)
    }catch(err){}

    const sendDataToConnectedSuperUsers = (data) => {
        return s.tx(data,'$')
    }
    const getFaceFolderNames = async () => {
        const folders = await fs.readdir(facesFolder)
        var faces = []
        for (let i = 0; i < folders.length; i++) {
            let folder = folders[i]
            var stats = await fs.stat(facesFolder + folder)
            if(stats.isDirectory()){
                faces.push(folder)
            }
        }
        return faces
    }
    const getUnknownFaceFolderNames = async () => {
        const folders = await fs.readdir(unknownFacesFolder)
        var faces = []
        for (let i = 0; i < folders.length; i++) {
            let folder = folders[i]
            var stats = await fs.stat(unknownFacesFolder + folder)
            if(stats.isDirectory()){
                faces.push(folder)
            }
        }
        return faces
    }
    const getFaceImages = async () => {
        const folders = await fs.readdir(facesFolder)
        var faces = {}
        for (let i = 0; i < folders.length; i++) {
            let name = folders[i]
            var stats = await fs.stat(facesFolder + name)
            if(stats.isDirectory()){
                faces[name] = []
                var images
                try{
                    images = await fs.readdir(facesFolder + name)
                }catch(err){
                    images = []
                }
                for (let i = 0; i < images.length; i++) {
                    let image = images[i]
                    faces[name].push(image)
                }
            }
        }
        return faces
    }
    const getUnknownFaceImages = async () => {
        const folders = await fs.readdir(unknownFacesFolder)
        var faces = {}
        for (let i = 0; i < folders.length; i++) {
            let name = folders[i]
            var stats = await fs.stat(unknownFacesFolder + name)
            if(stats.isDirectory()){
                faces[name] = []
                var images
                try{
                    images = await fs.readdir(unknownFacesFolder + name)
                }catch(err){
                    images = []
                }
                for (let i = 0; i < images.length; i++) {
                    let image = images[i]
                    faces[name].push(image)
                }
            }
        }
        return faces
    }
    const getFaceImagesByName = async (name) => {
        var stats = await fs.stat(facesFolder + name)
        var images = []
        if(stats.isDirectory()){
            try{
                images = await fs.readdir(facesFolder + name)
            }catch(err){
                images = []
            }
        }
        return images
    }
    const getUnknownFaceImagesByName = async (name) => {
        var stats = await fs.stat(unknownFacesFolder + name)
        var images = []
        if(stats.isDirectory()){
            try{
                images = await fs.readdir(unknownFacesFolder + name)
            }catch(err){
                images = []
            }
        }
        return images
    }
    const deletePath = async (deletionPath) => {
        try{
            await fs.rm(deletionPath,{ recursive: true, force: true })
        }catch(err){
            console.log(err)
        }
    }
    const getFaceFolderPrefix = (personName) => {
        const isUnknownFace = (personName.indexOf('UNKN_') > -1)
        return isUnknownFace ? unknownFacesFolder : facesFolder
    }
    const createFaceFolder = async (sanitizedName) => {
        const faceFolderPrefix = getFaceFolderPrefix(sanitizedName)
        try{
            await fs.mkdir(faceFolderPrefix + sanitizedName)
        }catch(err){
            console.log(err)
            return null
        }
        return faceFolderPrefix
    }
    const getFilenamePrefix = (imageFile) => {
        const fileParts = imageFile.split('.')
        delete(fileParts[fileParts.length - 1])
        const fileName = fileParts.filter((value) => {return !!value}).join('.')
        return fileName
    }
    app.get(config.webPaths.superApiPrefix+':auth/faceManager/names', function (req,res){
        s.superAuth(req.params, async function(resp){
            const faces = await getFaceFolderNames()
            const unknownFaces = await getUnknownFaceFolderNames()
            s.closeJsonResponse(res,{
                ok: true,
                faces: faces,
                unknownFaces: unknownFaces,
            })
        },res,req)
    })
    app.get(config.webPaths.superApiPrefix+':auth/faceManager/images', function (req,res){
        s.superAuth(req.params, async function(resp){
            const faces = await getFaceImages()
            const unknownFaces = await getUnknownFaceImages()
            s.closeJsonResponse(res,{
                ok: true,
                faces: faces,
                unknownFaces: unknownFaces,
            })
        },res,req)
    })
    app.get(config.webPaths.superApiPrefix+':auth/faceManager/image/:name/:image', function (req,res){
        s.superAuth(req.params, async function(resp){
            const imagePath = getFaceFolderPrefix(req.params.name) + req.params.name + '/' + req.params.image
            try{
                await fs.stat(imagePath)
                res.setHeader('Content-Type', 'image/jpeg')
                createReadStream(imagePath).pipe(res)
            }catch(err){
                console.log(err)
                s.closeJsonResponse(res,{
                    ok: false,
                    msg: lang['File Not Found']
                })
            }
        },res,req)
    })
    app.get(config.webPaths.superApiPrefix+':auth/faceManager/image/:name/:image/delete', function (req,res){
        s.superAuth(req.params, async function(resp){
            const imagePath = getFaceFolderPrefix(req.params.name) + req.params.name + '/' + req.params.image
            await deletePath(imagePath)
            sendDataToConnectedSuperUsers({
                f:'faceManagerImageDeleted',
                faceName: req.params.name,
                fileName: req.params.image,
            })
            const faces = await getFaceFolderNames()
            s.sendToAllDetectors({
                f: 'recompileFaceDescriptors',
                faces: faces
            })
            s.closeJsonResponse(res,{
                ok: true,
            })
        },res,req)
    })
    app.get(config.webPaths.superApiPrefix+':auth/faceManager/delete/:name', function (req,res){
        s.superAuth(req.params, async function(resp){
            const facePath = getFaceFolderPrefix(req.params.name) + req.params.name
            await deletePath(facePath)
            const faces = await getFaceFolderNames()
            const unknownFaces = await getUnknownFaceFolderNames()
            s.sendToAllDetectors({
                f: 'recompileFaceDescriptors',
                faces: faces,
                unknownFaces: unknownFaces,
            })
            sendDataToConnectedSuperUsers({
                f:'faceManagerFolderDeleted',
                faceName: req.params.name,
            })
            s.closeJsonResponse(res,{
                ok: true,
            })
        },res,req)
    })
    app.get(config.webPaths.superApiPrefix+':auth/faceManager/image/:name/:image/move/:newName/:newImage', function (req,res){
        s.superAuth(req.params,function(resp){
            s.sendToAllDetectors({
                f: 'moveFaceImage',
                personName: req.params.name,
                imageFile: req.params.image,
                newPersonName: req.params.newName,
                newImageFile: req.params.newImage,
            })
            if(req.query.websocketResponse){
                sendDataToConnectedSuperUsers({
                    f:'faceManagerImageDeleted',
                    faceName: req.params.name,
                    fileName: req.params.image,
                })
                var fileLink = config.webPaths.superApiPrefix + req.params.auth + `/faceManager/image/${req.params.newName}/${req.params.newImage}`
                sendDataToConnectedSuperUsers({
                    f:'faceManagerImageUploaded',
                    faceName: req.params.newName,
                    fileName: req.params.newImage,
                    url: fileLink
                })
            }
            s.closeJsonResponse(res,{
                ok: true,
            })
        },res,req)
    })
    app.get(config.webPaths.superApiPrefix+':auth/faceManager/moveFace/:name/:newName', function (req,res){
        s.superAuth(req.params,function(resp){
            s.sendToAllDetectorsWithCallback({
                f: 'moveFaceFolder',
                personName: req.params.name,
                newPersonName: req.params.newName,
            },(fileList) => {
                fileList.forEach((fileName) => {
                    if(req.query.websocketResponse){
                        sendDataToConnectedSuperUsers({
                            f:'faceManagerImageDeleted',
                            faceName: req.params.name,
                            fileName: fileName,
                        })
                        var fileLink = config.webPaths.superApiPrefix + req.params.auth + `/faceManager/image/${req.params.newName}/${fileName}`
                        sendDataToConnectedSuperUsers({
                            f:'faceManagerImageUploaded',
                            faceName: req.params.newName,
                            fileName: fileName,
                            url: fileLink
                        })
                    }
                })
            })
            s.closeJsonResponse(res,{
                ok: true,
            })
        },res,req)
    })
    app.get(config.webPaths.superApiPrefix+':auth/faceManager/create/:name', function (req,res){
        s.superAuth(req.params, async function(resp){
            s.closeJsonResponse(res,{ok: !!await createFaceFolder(req.params.name)})
        },res,req)
    })
    app.post(config.webPaths.superApiPrefix+':auth/faceManager/image/:name', fileUpload(), async function (req,res){
        s.superAuth(req.params, async function(resp){
            var fileKeys = Object.keys(req.files || {})
            if(fileKeys.length == 0){
                return res.status(400).send('No files were uploaded.')
            }
            var filesUploaded = []
            var checkFile = async (file) => {
                if(file.name.indexOf('.jpg') > -1 || file.name.indexOf('.jpeg') > -1){
                    const sanitizedName = req.params.name.replace('UNKN_','')
                    filesUploaded.push(file.name)
                    const faceFolderPrefix = await createFaceFolder(sanitizedName)
                    file.mv(faceFolderPrefix + sanitizedName + '/' + file.name, function(err) {
                        var fileLink = config.webPaths.superApiPrefix + req.params.auth + `/faceManager/image/${sanitizedName}/${file.name}`
                        sendDataToConnectedSuperUsers({
                            f:'faceManagerImageUploaded',
                            faceName: sanitizedName,
                            fileName: file.name,
                            url: fileLink
                        })
                    })
                }
            }
            for (let i = 0; i < fileKeys.length; i++) {
                let key = fileKeys[i]
                var file = req.files[key]
                try{
                    if(file instanceof Array){
                        for (let i = 0; i < file.length; i++) {
                            let fileOfFile = file[i]
                            await checkFile(fileOfFile)
                        }
                    }else{
                        await checkFile(file)
                    }
                }catch(err){
                    console.log(file)
                    console.log(err)
                }
            }
            var response = {
                ok: true,
                filesUploaded: filesUploaded
            }
            s.closeJsonResponse(res,response)
            const faces = await getFaceFolderNames()
            const unknownFaces = await getUnknownFaceFolderNames()
            s.sendToAllDetectors({
                f: 'recompileFaceDescriptors',
                faces: faces,
                unknownFaces: unknownFaces,
            })
        },res,req)
    })
}
